<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="main-body">
	<div class="page-body">
		<div class="row">
			<div class="col-md-12 mb-2">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item"><a href="/admin"> <i
								class="feather icon-home"></i> Trang chủ
						</a></li>
						<li class="breadcrumb-item"><a
							href='<c:url value="<%=UrlConstants.URL_COURSE%>"/>'>Danh
								sách khóa học</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="card px-3">
					<div class="card-header px-0 pb-2">
						<h2 class="text-uppercase text-center">Danh sách khóa học</h2>
						<a
							href='<c:url value="<%=UrlConstants.URL_COURSE + UrlConstants.URL_ADD%>"/>'
							class="btn btn-sm btn-primary">Thêm mới</a>
					</div>
					<div class="card-block table-border-style">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>STT</th>
										<th>Tiêu Đề</th>
										<th>Hình Đại Diện</th>
										<th>Số Bài Học</th>
										<th>Giá Bán</th>
										<th>Hành Động</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="item" items="${listCourseDto }">
										<tr>
											<th>${item.id }</th>
											<td>${item.title }</td>
											<td><img src='<c:url value="/images/1.png"/>'
												height="50" class="p-1 border" /></td>
											<td>${item.letures_count }</td>
											<td class="text-danger font-weight-bold"><fmt:formatNumber
													value="${item.price}" minFractionDigits="0" /> VNĐ</td>
											<td><a
												href='<c:url value="<%=UrlConstants.URL_COURSE+UrlConstants.URL_EDIT %>"/>?id=${item.id}'
												class="btn btn-sm btn-info btn-round py-1 font-weight-bold">Sửa</a>
												<a
												href='<c:url value="<%=UrlConstants.URL_COURSE+UrlConstants.URL_DELETE %>"/>?id=${item.id}'
												class="btn btn-sm btn-danger btn-round py-1 font-weight-bold">Xóa</a>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>