<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="main-body">
	<div class="page-body">
		<div class="row">
			<div class="col-md-12 mb-2">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item"><a href="/admin"> <i
								class="feather icon-home"></i> Trang chủ
						</a></li>
						<li class="breadcrumb-item"><a
							href='<c:url value="<%=UrlConstants.URL_COURSE%>" />'> <i
								class="feather icon-home"></i> Danh sách khóa học
						</a></li>
						<li class="breadcrumb-item"><a
							href='<c:url value="<%=UrlConstants.URL_COURSE + UrlConstants.URL_ADD%>" />'>Thêm
								mới khóa học</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						<h2 class="text-uppercase text-center">Thêm mới khóa học</h2>
						<p class="text-center text-danger">${message }</p>
					</div>
					<div class="card-block">
						<c:url value="<%=UrlConstants.URL_COURSE + UrlConstants.URL_ADD%>"
							var="action" />
						<form:form action="${action }" method="POST"
							modelAttribute="courseDto">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Tiêu đề</label>
										<form:input path="title" cssClass="form-control" />
										<form:errors path="title" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Số bài học</label>
										<form:input type="number" path="letures_count"
											cssClass="form-control" />
										<form:errors path="letures_count" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Số giờ học</label>
										<form:input type="number" path="hour_count"
											cssClass="form-control" />
										<form:errors path="hour_count" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Danh mục</label>
										<form:select path="category_id" cssClass="form-control"
											items="${listCategory }" itemValue="id" itemLabel="title" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Giá bán</label>
										<form:input path="price" cssClass="form-control" />
										<form:errors path="price" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Giảm giá (%)</label>
										<form:input type="number" path="discount"
											cssClass="form-control" />
										<form:errors path="discount" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Mô tả</label>
										<form:input path="description" cssClass="form-control" />
									</div>
									<div class="form-group">
										<label>Hình đại diện (Optional)</label>
										<form:hidden path="image" />
										<div id="dZUpload" class="dropzone"
											data-upload='<c:url value="<%=UrlConstants.URL_FILE + UrlConstants.URL_UPLOAD%>"/>'
											data-load='<c:url value="<%=UrlConstants.URL_FILE + UrlConstants.URL_LOAD%>"/>'>
											<div class="dz-default dz-message"></div>
										</div>
									</div>
								</div>
								<div class="col-12">
									<label>Nội dung</label>
									<form:textarea path="content" cssClass="form-control" rows="5" />
								</div>
								<div class="col-12 mt-3">
									<div class="form-group">
										<button type="submit" class="btn btn-primary m-b-0">Lưu
											lại</button>
										<a href='<c:url value="<%=UrlConstants.URL_COURSE%>" />'
											class="btn btn-secondary text-white">Quay lại</a>
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>