<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="main-body">
	<div class="page-body">
		<div class="row">
			<div class="col-md-12 mb-2">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item"><a href="/admin"> <i
								class="feather icon-home"></i> Trang chủ
						</a></li>
						<li class="breadcrumb-item"><a
							href='<c:url value="<%=UrlConstants.URL_USER%>" />?id=${user.id}'>
								<i class="feather icon-home"></i> Danh sách tài khoản
						</a></li>
						<li class="breadcrumb-item"><a
							href='<c:url value="<%=UrlConstants.URL_USER + UrlConstants.URL_EDIT%>" />?id=${user.id}'>Sửa
								thông tin tài khoản</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						<h2 class="text-uppercase text-center">Sửa thông tin tài
							khoản</h2>
						<p class="text-center text-danger">${message }</p>
					</div>
					<div class="card-block">
						<c:url value="<%=UrlConstants.URL_USER + UrlConstants.URL_EDIT%>"
							var="action" />
						<form:form action="${action }" method="POST" modelAttribute="userDto">
							<div class="row">
								<form:hidden path="id" />
								<div class="col-md-6">
									<div class="form-group">
										<label>Họ tên</label>
										<form:input path="fullname" cssClass="form-control" />
										<form:errors path="fullname" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Email</label>
										<form:input readonly="true" path="email"
											cssClass="form-control" />
									</div>
									<div class="form-group">
										<label>Mật khẩu</label>
										<form:password path="password" cssClass="form-control" />
										<form:errors path="password" cssClass="text-danger" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Điện thoại (Optional)</label>
										<form:input path="phone" cssClass="form-control" />
									</div>
									<div class="form-group">
										<label>Địa chỉ (Optional)</label>
										<form:input path="address" cssClass="form-control" />
									</div>
									<div class="form-group">
										<label>Avatar (Optional)</label>
										<form:hidden path="avatar" />
										<div id="dZUpload" class="dropzone"
											data-upload='<c:url value="<%=UrlConstants.URL_FILE + UrlConstants.URL_UPLOAD%>"/>'
											data-load='<c:url value="<%=UrlConstants.URL_FILE + UrlConstants.URL_LOAD%>"/>'>
											<div class="dz-default dz-message"></div>
										</div>
									</div>
									<div class="form-group">
										<label>Loại người dùng</label>
										<form:select path="role_id" items="${listRoleDto }"
											itemLabel="name" itemValue="id" cssClass="form-control" />
									</div>
								</div>
								<div class="col-12 mt-3">
									<div class="form-group">
										<button type="submit" class="btn btn-primary m-b-0">Lưu
											lại</button>
										<a href='<c:url value="<%=UrlConstants.URL_USER%>" />'
											class="btn btn-secondary text-white">Quay lại</a>
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>