<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="icon" href='<c:url value="/images/favicon.ico" />'
	type="image/x-icon" />
<!-- Google font-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600"
	rel="stylesheet" />
<!-- Required Fremwork -->
<link rel="stylesheet" type="text/css"
	href='<c:url value="/plugins/bootstrap/css/bootstrap.min.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/plugins/font-awesome/css/font-awesome.min.css" />' />
<!-- themify-icons line icon -->
<link rel="stylesheet" type="text/css"
	href='<c:url value="/icon/themify-icons/themify-icons.css" />' />
<!-- ico font -->
<link rel="stylesheet" type="text/css"
	href='<c:url value="/icon/icofont/css/icofont.css" />' />
<!-- feather Awesome -->
<link rel="stylesheet" type="text/css"
	href='<c:url value="/icon/feather/css/feather.css" />' />
<!-- Style.css -->
<link rel="stylesheet" type="text/css"
	href='<c:url value="/css/style.css" />' />
<link rel="stylesheet" type="text/css"
	href='<c:url value="/css/jquery.mCustomScrollbar.css" />' />
<!-- dropzone -->
<link rel="stylesheet" type="text/css"
	href='<c:url value="/css/dropzone.min.css" />' />
