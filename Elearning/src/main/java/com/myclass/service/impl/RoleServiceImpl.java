package com.myclass.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.myclass.dto.RoleDto;
import com.myclass.entity.Role;
import com.myclass.service.RoleService;

@Service
public class RoleServiceImpl extends GenericServiceImpl<Role, RoleDto> implements RoleService {

	public List<RoleDto> search(String keyword) {
		return this.mappingListEntityToListDto(this.genericRepository.search(keyword));
	}

}
