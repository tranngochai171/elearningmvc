package com.myclass.service.impl;

import java.lang.reflect.ParameterizedType;
import java.util.LinkedList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.myclass.repository.GenericRepository;

import com.myclass.service.GenericService;

public abstract class GenericServiceImpl<TEntity, TDto> implements GenericService<TEntity, TDto> {
	@Autowired
	protected GenericRepository<TEntity> genericRepository;
	protected ModelMapper modelMapper;

	private Class<? extends TEntity> clazzEntity;
	private Class<? extends TDto> clazzDto;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GenericServiceImpl() {
		modelMapper = new ModelMapper();
		ParameterizedType pt = (ParameterizedType) getClass().getGenericSuperclass();
		this.clazzEntity = (Class) pt.getActualTypeArguments()[0];
		this.clazzDto = (Class) pt.getActualTypeArguments()[1];
	}

	public List<TDto> findAll() {
		List<TEntity> listEntity = this.genericRepository.findAll();
		if (listEntity != null)
			return this.mappingListEntityToListDto(listEntity);
		else
			return null;
	}

	public boolean saveOrUpdate(TDto dto) {
		TEntity entity = this.modelMapper.map(dto, clazzEntity);
		return this.genericRepository.saveOrUpdate(entity);
	}

	public boolean delete(Integer id) {
		return this.genericRepository.delete(id);
	}

	public TDto findById(Integer id) {
		TEntity entity = this.genericRepository.findById(id);
		if (entity != null)
			return this.modelMapper.map(entity, clazzDto);
		else
			return null;
	}

	public List<TDto> mappingListEntityToListDto(List<TEntity> listEntity) {
		List<TDto> listDto = new LinkedList<TDto>();
		for (TEntity item : listEntity) {
			listDto.add(this.modelMapper.map(item, clazzDto));
		}
		return listDto;
	}

}
