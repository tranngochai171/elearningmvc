package com.myclass.service;

import com.myclass.dto.UserDto;
import com.myclass.entity.User;

public interface UserService extends GenericService<User, UserDto> {

	public UserDto findByEmail(String email);
}
