package com.myclass.repository.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.myclass.repository.GenericRepository;

@Transactional(rollbackOn = Exception.class)
public abstract class GenericRepositoryImpl<T> implements GenericRepository<T> {
	@Autowired
	protected SessionFactory sessionFactory;

	private Class<? extends T> clazz;
	private String entityName;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GenericRepositoryImpl() {
		ParameterizedType pt = (ParameterizedType) getClass().getGenericSuperclass();
		this.clazz = (Class) pt.getActualTypeArguments()[0];
		this.entityName = this.clazz.getSimpleName();
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String hql = "FROM " + this.entityName;
			return (List<T>) session.createQuery(hql, this.clazz).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean saveOrUpdate(T entity) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.saveOrUpdate(entity);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean delete(Integer id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			T entity = session.find(this.clazz, id);
			if (entity != null) {
				session.remove(entity);
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public T findById(Integer id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			return session.find(this.clazz, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
