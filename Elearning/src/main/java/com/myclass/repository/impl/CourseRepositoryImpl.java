package com.myclass.repository.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.myclass.entity.Course;
import com.myclass.repository.CourseRepository;

@Repository
@Transactional(rollbackOn = Exception.class)
public class CourseRepositoryImpl extends GenericRepositoryImpl<Course> implements CourseRepository {

	public List<Course> findPaging(int pageIndex, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Course> search(String keyword) {
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "FROM Course WHERE title LIKE :title";
		Query<Course> query = session.createQuery(hql, Course.class);
		query.setParameter("title", "%" + keyword + "%");
		return query.getResultList();
	}

	public List<Course> search(String keyword, int pageIndex, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

}
