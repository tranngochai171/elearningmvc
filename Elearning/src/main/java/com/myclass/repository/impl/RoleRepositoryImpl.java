package com.myclass.repository.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.myclass.entity.Role;
import com.myclass.repository.RoleRepository;

@Repository
@Transactional(rollbackOn = Exception.class)
public class RoleRepositoryImpl extends GenericRepositoryImpl<Role> implements RoleRepository {

	public List<Role> search(String keyword) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String hql = "FROM Role WHERE name LIKE :name";
			Query<Role> query = session.createQuery(hql, Role.class);
			query.setParameter("name", "%" + keyword + "%");
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Role> findPaging(int pageIndex, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Role> search(String keyword, int pageIndex, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

}
