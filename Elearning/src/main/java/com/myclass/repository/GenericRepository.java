package com.myclass.repository;

import java.util.List;

public interface GenericRepository<T> {
	public List<T> findAll();

	public boolean saveOrUpdate(T entity);

	public boolean delete(Integer id);

	public T findById(Integer id);

	public List<T> findPaging(int pageIndex, int pageSize);

	public abstract List<T> search(String keyword);

	public abstract List<T> search(String keyword, int pageIndex, int pageSize);

}
