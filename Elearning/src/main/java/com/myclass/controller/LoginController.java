package com.myclass.controller;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myclass.dto.LoginUserDto;
import com.myclass.dto.UserDto;
import com.myclass.service.UserService;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_LOGIN)
public class LoginController {
	@Autowired
	private UserService userService;

	@GetMapping(value = "")
	public String index(ModelMap model) {
		model.addAttribute("loginUserDto", new LoginUserDto());
		return PathConstants.PATH_LOGIN;
	}

	@PostMapping(value = "")
	public String index(ModelMap model, @Validated @ModelAttribute("loginUserDto") LoginUserDto loginUserDto,
			BindingResult errors) {
		UserDto user = this.userService.findByEmail(loginUserDto.getEmail());
		if (errors.hasErrors() || user == null || !BCrypt.checkpw(loginUserDto.getPassword(), user.getPassword())) {
			model.addAttribute("loginUserDto", loginUserDto);
			model.addAttribute("message", "(*) Đăng nhập thất bại");
			return PathConstants.PATH_LOGIN;
		}
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_ROLE;
	}
}
