package com.myclass.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.myclass.dto.CategoryDto;
import com.myclass.service.CategoryService;
import com.myclass.util.CommonMethodsAndAttributes;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_CATEGORY)
public class CategoryController {
	@Autowired
	private CategoryService categoryService;

	@GetMapping(value = "")
	public String index(ModelMap model, HttpSession session) {
		CommonMethodsAndAttributes.showMessage(session, model);
		model.addAttribute("listCategoryDto", this.categoryService.findAll());
		return PathConstants.PATH_CATGORY;
	}

	@GetMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model) {
		model.addAttribute("categoryDto", new CategoryDto());
		return PathConstants.PATH_CATGORY_ADD;
	}

	@PostMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model, HttpSession session,
			@Validated @ModelAttribute("categoryDto") CategoryDto categoryDto, BindingResult errors) {
		if (errors.hasErrors() || !this.categoryService.saveOrUpdate(categoryDto)) {
			model.addAttribute("categoryDto", categoryDto);
			model.addAttribute("message", "(*) Thêm mới thất bại");
			return PathConstants.PATH_CATGORY_ADD;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Thêm mới thành công");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_CATEGORY;
	}

	@GetMapping(value = UrlConstants.URL_DELETE)
	public String delete(@RequestParam("id") int id, HttpSession session) {
		if (!this.categoryService.delete(id))
			session.setAttribute(CommonMethodsAndAttributes.MSG_ERROR, "Xóa thất bại");
		else
			session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Xóa thành công");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_CATEGORY;
	}

	@GetMapping(value = UrlConstants.URL_EDIT)
	public String edit(ModelMap model, @RequestParam("id") int id, HttpSession session) {
		CategoryDto categoryDto = this.categoryService.findById(id);
		if (categoryDto != null) {
			model.addAttribute("categoryDto", categoryDto);
			return PathConstants.PATH_CATGORY_EDIT;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_ERROR, "Không tìm thấy Category");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_CATEGORY;
	}

	@PostMapping(value = UrlConstants.URL_EDIT)
	public String edit(HttpSession session, ModelMap model,
			@Validated @ModelAttribute("categoryDto") CategoryDto categoryDto, BindingResult errors) {
		if (errors.hasErrors() || !this.categoryService.saveOrUpdate(categoryDto)) {
			model.addAttribute("message", "(*) Sửa thất bại");
			model.addAttribute("category", categoryDto);
			return PathConstants.PATH_CATGORY_EDIT;
		}
		session.setAttribute(CommonMethodsAndAttributes.MSG_SUCCESS, "Sửa thành công");
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_CATEGORY;
	}

	@GetMapping(value = UrlConstants.URL_SEARCH)
	public String search(@RequestParam("keyword") String keyword, ModelMap model) {
		model.addAttribute("listCategoryDto", this.categoryService.search(keyword));
		return PathConstants.PATH_CATGORY;
	}
}
