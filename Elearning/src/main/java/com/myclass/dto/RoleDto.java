package com.myclass.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

public class RoleDto {
	private int id;
	@NotBlank
	private String name;
	@NotBlank
	private String description;

	private List<UserDto> users;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<UserDto> getUsers() {
		return users;
	}

	public void setUsers(List<UserDto> users) {
		this.users = users;
	}

	public RoleDto() {

	}

}
