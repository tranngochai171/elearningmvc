package com.myclass.dto;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

public class CourseDto {
	private Integer id;
	@NotBlank
	private String title;
	private String image;
	@NotNull
	@Min(value = 0)
	private Integer letures_count;
	@NotNull
	@Min(value = 0)
	private Integer hour_count;
	private Integer view_count;
	@NotNull
	@Min(value = 0)
	private Float price;
	@NotNull
	@Range(min = 0, max = 100)
	private Integer discount;
	private Float promotion_price;
	private String description;
	private String content;
	private Integer category_id;
	private Date last_update;

	private CategoryDto category;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getLetures_count() {
		return letures_count;
	}

	public void setLetures_count(Integer letures_count) {
		this.letures_count = letures_count;
	}

	public Integer getHour_count() {
		return hour_count;
	}

	public void setHour_count(Integer hour_count) {
		this.hour_count = hour_count;
	}

	public Integer getView_count() {
		return view_count;
	}

	public void setView_count(Integer view_count) {
		this.view_count = view_count;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Float getPromotion_price() {
		return promotion_price;
	}

	public void setPromotion_price(Float promotion_price) {
		this.promotion_price = promotion_price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Integer category_id) {
		this.category_id = category_id;
	}

	public Date getLast_update() {
		return last_update;
	}

	public void setLast_update(Date last_update) {
		this.last_update = last_update;
	}

	public CategoryDto getCategory() {
		return category;
	}

	public void setCategory(CategoryDto category) {
		this.category = category;
	}

}
