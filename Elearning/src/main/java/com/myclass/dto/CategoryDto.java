package com.myclass.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

import com.myclass.entity.Course;

public class CategoryDto {
	private int id;
	@NotBlank
	private String title;
	private String icon;
	private List<Course> courses;

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
