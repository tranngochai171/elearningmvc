package com.myclass.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.myclass.dto.UserDto;


@Component
public class UserValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return UserDto.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		UserDto user = (UserDto) target;
		if (user.getPassword().length() < 5 || user.getPassword().length() > 15)
			errors.rejectValue("password", "user", "(*) Mật khẩu phải có độ dài từ 5 - 15 ký tự");
	}

}
