package com.myclass.util;

public class PathConstants {
	// ERROR VIEW
	public static final String PATH_ERROR_404 = "error-404";
	public static final String PATH_ERROR_400 = "error-400";
	// ROLE VIEW
	public static final String PATH_ROLE = "role";
	public static final String PATH_ROLE_ADD = "role-add";
	public static final String PATH_ROLE_EDIT = "role-edit";
	// USER VIEW
	public static final String PATH_USER = "user";
	public static final String PATH_USER_ADD = "user-add";
	public static final String PATH_USER_EDIT = "user-edit";
	// CATGORY VIEW
	public static final String PATH_CATGORY = "category";
	public static final String PATH_CATGORY_ADD = "category-add";
	public static final String PATH_CATGORY_EDIT = "category-edit";
	// COURSE VIEW
	public static final String PATH_COURSE = "course";
	public static final String PATH_COURSE_ADD = "course-add";
	public static final String PATH_COURSE_EDIT = "course-edit";
	// VIDEO VIEW
	public static final String PATH_VIDEO = "video";
	public static final String PATH_VIDEO_ADD = "video-add";
	public static final String PATH_VIDEO_EDIT = "video-edit";
	// LOGIN VIEW
	public static final String PATH_LOGIN = "login";
}
