package com.myclass.util;

public class UrlConstants {
	// COMMON URL
	public static final String URL_REDIRECT = "redirect:";
	public static final String URL_ADD = "/add";
	public static final String URL_DELETE = "/delete";
	public static final String URL_EDIT = "/edit";
	// LOGIN CONTROLLER
	public static final String URL_LOGIN = "/login";
	// CATEGORY CONTROLLER
	public static final String URL_CATEGORY = "/category";

	// COURSE CONTROLLER
	public static final String URL_COURSE = "/course";

	// VIDEO CONTROLLER
	public static final String URL_VIDEO = "/video";

	// TARGET CONTROLLER
	public static final String URL_TARGET = "/target";

	// ROLE CONTROLLER
	public static final String URL_ROLE = "/role";

	// USER CONTROLLER
	public static final String URL_USER = "/user";

	// USER CONTROLLER
	public static final String URL_ERROR = "/error";
	public static final String URL_ERROR_404 = "/404";
	public static final String URL_ERROR_400 = "/400";
}
