package com.myclass.util;

public class PathConstants {
	// CATEGORY VIEW
	public static final String PATH_LOGIN = "login";
	// CATEGORY VIEW
	public static final String PATH_CATEGORY = "category";
	public static final String PATH_CATEGORY_ADD = "category_add";
	public static final String PATH_CATEGORY_EDIT = "category_edit";
	// COURSE VIEW
	public static final String PATH_COURSE = "course";
	public static final String PATH_COURSE_ADD = "course_add";
	// VIDEO VIEW
	public static final String PATH_VIDEO = "video";
	public static final String PATH_VIDEO_ADD = "video_add";
	// TARGET VIEW
	public static final String PATH_TARGET = "target";
	public static final String PATH_TARGET_ADD = "target_add";
	// ROLE VIEW
	public static final String PATH_ROLE = "role";
	public static final String PATH_ROLE_ADD = "role_add";
	public static final String PATH_ROLE_EDIT = "role_edit";
	// USER VIEW
	public static final String PATH_USER = "user";
	public static final String PATH_USER_ADD = "user_add";
	public static final String PATH_USER_EDIT = "user_edit";
	// ERROR
	public static final String PATH_404 = "error_404";
	public static final String PATH_400 = "error_400";
}
