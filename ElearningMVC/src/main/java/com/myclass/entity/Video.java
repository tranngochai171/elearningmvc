package com.myclass.entity;

public class Video {
	private int id;
	private String title;
	private String url;
	private int time_count;
	private int course_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getTime_count() {
		return time_count;
	}

	public void setTime_count(int time_count) {
		this.time_count = time_count;
	}

	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}
}
