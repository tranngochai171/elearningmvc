package com.myclass.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.myclass.dto.UserDto;
import com.myclass.service.UserService;

@Component
public class UserValidator implements Validator {
	@Autowired
	private UserService userService;

	public boolean supports(Class<?> clazz) {
		return UserDto.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		UserDto userDto = (UserDto) target;
		if (!userDto.getPassword().equals(userDto.getPassword_comfirm())) {
			errors.rejectValue("password_comfirm", "userDto", "(*) Mật khẩu chưa trùng");
		}
		if (this.userService.findUserByEmail(userDto.getEmail()) != null) {
			errors.rejectValue("email", "userDto", "(*) Email đã trùng, vui lòng nhập email khác");
		}
	}

}
