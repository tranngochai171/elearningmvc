package com.myclass.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.myclass.dto.UserDto;

@Component
public class UserEditValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return UserDto.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		UserDto userDto = (UserDto) target;
		if (!userDto.getPassword().equals(userDto.getPassword_comfirm())) {
			errors.rejectValue("password_comfirm", "userDto", "(*) Mật khẩu chưa trùng");
		}
	}

}
