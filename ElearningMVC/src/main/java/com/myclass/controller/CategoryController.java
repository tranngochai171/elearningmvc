package com.myclass.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.myclass.entity.Category;
import com.myclass.service.CategoryService;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_CATEGORY)
public class CategoryController {
	@Autowired
	private CategoryService categoryService;

	@GetMapping(value = "")
	public String index(ModelMap model) {
		model.addAttribute("listCategory", this.categoryService.getListCategory());
		return PathConstants.PATH_CATEGORY;
	}

	@GetMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model) {
		model.addAttribute("category", new Category());
		return PathConstants.PATH_CATEGORY_ADD;
	}

	@PostMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model, @Validated @ModelAttribute("category") Category category, BindingResult errors) {
		if (errors.hasErrors() || this.categoryService.addNewCategory(category) <= 0) {
			model.addAttribute("message", "(*) Thêm mới thất bại");
			model.addAttribute("category", category);
			return PathConstants.PATH_CATEGORY_ADD;
		}
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_CATEGORY;
	}

	@GetMapping(value = UrlConstants.URL_DELETE)
	public String delete(@RequestParam("id") int id) {
		this.categoryService.deleteCategoryById(id);
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_CATEGORY;
	}

	@GetMapping(value = UrlConstants.URL_EDIT)
	public String edit(@RequestParam("id") int id, ModelMap model) {
		model.addAttribute("category", this.categoryService.findById(id));
		return PathConstants.PATH_CATEGORY_EDIT;
	}

	@PostMapping(value = UrlConstants.URL_EDIT)
	public String edit(ModelMap model, @Validated @ModelAttribute("category") Category category, BindingResult errors) {
		if (errors.hasErrors() || this.categoryService.editCategory(category) <= 0) {
			model.addAttribute("message", "(*) Chỉnh sửa thất bại");
			model.addAttribute("category", category);
			return PathConstants.PATH_CATEGORY_EDIT;
		}
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_CATEGORY;
	}
}
