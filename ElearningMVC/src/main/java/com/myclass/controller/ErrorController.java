package com.myclass.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_ERROR)
public class ErrorController {
	@GetMapping(value = UrlConstants.URL_ERROR_404)
	public String notFound404() {
		return PathConstants.PATH_404;
	}

	@GetMapping(value = UrlConstants.URL_ERROR_400)
	public String invalid400() {
		return PathConstants.PATH_400;
	}
}
