package com.myclass.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.myclass.entity.Role;
import com.myclass.service.RoleService;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_ROLE)
public class RoleController {
	@Autowired
	private RoleService roleService;

	@GetMapping(value = "")
	public String index(ModelMap model) {
		model.addAttribute("listRole", this.roleService.getListRole());
		return PathConstants.PATH_ROLE;
	}

	@GetMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model) {
		model.addAttribute("role", new Role());
		return PathConstants.PATH_ROLE_ADD;
	}

	@PostMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model, @Validated @ModelAttribute("role") Role role, BindingResult errors) {
		if (errors.hasErrors() || this.roleService.addNewRole(role) <= 0) {
			model.addAttribute("role", role);
			model.addAttribute("message", "(*) Thêm mới role thất bại");
			return PathConstants.PATH_ROLE_ADD;
		}
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_ROLE;
	}

	@GetMapping(value = UrlConstants.URL_DELETE)
	public String delete(@RequestParam("id") int id) {
		this.roleService.deleteRoleById(id);
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_ROLE;
	}

	@GetMapping(value = UrlConstants.URL_EDIT)
	public String edit(@RequestParam("id") int id, ModelMap model) {
		model.addAttribute("role", this.roleService.findById(id));
		return PathConstants.PATH_ROLE_EDIT;
	}

	@PostMapping(value = UrlConstants.URL_EDIT)
	public String edit(ModelMap model, @Validated @ModelAttribute("role") Role role, BindingResult errors) {
		if (errors.hasErrors() || this.roleService.editRole(role) <= 0) {
			model.addAttribute("message", "Cập nhật mới thất bại");
			model.addAttribute("role", role);
			return PathConstants.PATH_ROLE_EDIT;
		}
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_ROLE;
	}
}
