package com.myclass.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myclass.service.CourseService;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_COURSE)
public class CourseController {
	@Autowired
	private CourseService courseService;

	@GetMapping(value = "")
	public String index(ModelMap model) {
		model.addAttribute("listCourse", this.courseService.getListCourse());
		return PathConstants.PATH_COURSE;
	}

	@GetMapping(value = UrlConstants.URL_ADD)
	public String add() {
		return PathConstants.PATH_COURSE_ADD;
	}
}
