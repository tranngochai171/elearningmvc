package com.myclass.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.myclass.dto.UserDto;
import com.myclass.entity.User;
import com.myclass.service.RoleService;
import com.myclass.service.UserService;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;
import com.myclass.validator.UserEditValidator;
import com.myclass.validator.UserValidator;

@Controller
@RequestMapping(value = UrlConstants.URL_USER)
public class UserController {
	@Autowired
	private RoleService roleService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserValidator validator;
	@Autowired
	private UserEditValidator editValidator;

	@GetMapping(value = "")
	public String index(ModelMap model) {
		model.addAttribute("listUserDto", this.userService.getListUserDto());
		return PathConstants.PATH_USER;
	}

	@GetMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model) {
		model.addAttribute("userDto", new UserDto());
		model.addAttribute("listRole", this.roleService.getListRole());
		return PathConstants.PATH_USER_ADD;
	}

	@PostMapping(value = UrlConstants.URL_ADD)
	public String add(ModelMap model, @Validated @ModelAttribute("userDto") UserDto userDto, BindingResult errors) {
		this.validator.validate(userDto, errors);
		User user = new User(userDto);
		if (errors.hasErrors() || this.userService.addNewUser(user) <= 0) {
			model.addAttribute("userDto", userDto);
			model.addAttribute("message", "(*) Thêm mới thất bại");
			model.addAttribute("listRole", this.roleService.getListRole());
			return PathConstants.PATH_USER_ADD;
		}
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_USER;
	}

	@GetMapping(value = UrlConstants.URL_DELETE)
	public String delete(@RequestParam("id") int id) {
		this.userService.deleteUserById(id);
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_USER;
	}

	@GetMapping(value = UrlConstants.URL_EDIT)
	public String edit(@RequestParam("id") int id, ModelMap model) {
		UserDto userDto = this.userService.findUserDtoById(id);
		if (userDto == null)
			return UrlConstants.URL_REDIRECT + UrlConstants.URL_USER;
		model.addAttribute("userDto", userDto);
		model.addAttribute("listRole", this.roleService.getListRole());
		return PathConstants.PATH_USER_EDIT;
	}

	@PostMapping(value = UrlConstants.URL_EDIT)
	public String edit(ModelMap model, @RequestParam("id") int id,
			@Validated @ModelAttribute("userDto") UserDto userDto, BindingResult errors) {
		this.editValidator.validate(userDto, errors);
		User user = new User(userDto);
		if (errors.hasErrors() || this.userService.editUser(user) <= 0) {
			model.addAttribute("message", "(*) Sửa thông tin người dùng thất bại");
			model.addAttribute("userDto", userDto);
			model.addAttribute("listRole", this.roleService.getListRole());
			return PathConstants.PATH_USER_EDIT;
		}

		return UrlConstants.URL_REDIRECT + UrlConstants.URL_USER;
	}

}
