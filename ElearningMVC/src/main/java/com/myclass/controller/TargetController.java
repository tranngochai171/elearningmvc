package com.myclass.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_TARGET)
public class TargetController {
	@GetMapping(value = "")
	public String index() {
		return PathConstants.PATH_TARGET;
	}

	@GetMapping(value = UrlConstants.URL_ADD)
	public String add() {
		return PathConstants.PATH_TARGET_ADD;
	}
}
