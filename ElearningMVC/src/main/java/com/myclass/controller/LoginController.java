package com.myclass.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myclass.entity.User;
import com.myclass.service.UserService;
import com.myclass.util.PathConstants;
import com.myclass.util.UrlConstants;

@Controller
@RequestMapping(value = UrlConstants.URL_LOGIN)
public class LoginController {
	@Autowired
	private UserService userService;

	@GetMapping(value = "")
	public String index(ModelMap model) {
		model.addAttribute("user", new User());
		return PathConstants.PATH_LOGIN;
	}

	@PostMapping(value = "")
	public String index(ModelMap model, @Validated @ModelAttribute("user") User user, BindingResult errors) {
		User user_check = this.userService.findUserByEmail(user.getEmail());
		if (errors.hasErrors() || user_check == null || !user_check.getPassword().equals(user.getPassword())) {
			model.addAttribute("user", user);
			model.addAttribute("message", "(*) Đăng nhập thất bại");
			return PathConstants.PATH_LOGIN;
		}
		return UrlConstants.URL_REDIRECT + UrlConstants.URL_CATEGORY;
	}
}
