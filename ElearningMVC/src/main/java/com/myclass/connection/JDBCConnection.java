package com.myclass.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
	private static final String DATABASE = "jdbc:mysql://localhost:3306/elearning";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "password";

	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			return DriverManager.getConnection(DATABASE, USERNAME, PASSWORD);
		} catch (ClassNotFoundException e) {
			System.out.println("Khong tim thay Driver");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Khong tim thay Database");
			e.printStackTrace();
		}
		return null;
	}
}
