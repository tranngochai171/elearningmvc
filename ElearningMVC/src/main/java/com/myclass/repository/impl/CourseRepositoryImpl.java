package com.myclass.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.myclass.connection.JDBCConnection;
import com.myclass.entity.Course;
import com.myclass.repository.CourseRepository;

@Repository
public class CourseRepositoryImpl implements CourseRepository {

	public List<Course> getListCourse() {
		List<Course> listCourse = new LinkedList<Course>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM courses;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				Course course = new Course();
				course.setId(res.getInt("id"));
				course.setTitle(res.getString("title"));
				course.setImage(res.getString("image"));
				course.setLetures_count(res.getInt("letures_count"));
				course.setHour_count(res.getInt("hour_count"));
				course.setView_count(res.getInt("view_count"));
				course.setPrice(res.getFloat("price"));
				course.setDiscount(res.getInt("discount"));
				course.setPromotion_price(res.getFloat("promotion_price"));
				course.setDescription(res.getString("description"));
				course.setContent(res.getString("content"));
				course.setCategory_id(res.getInt("category_id"));
				course.setLast_update(res.getDate("last_update"));
				listCourse.add(course);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listCourse;
	}

	public int addNewCourse(Course course) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO categories (title,image,lectures_count,hour_count,"
					+ "view_count,price,discount,promotion_count,description,content,category_id,last_update)"
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, course.getTitle());
			stm.setString(2, course.getImage());
			stm.setInt(3, course.getLetures_count());
			stm.setInt(4, course.getHour_count());
			stm.setInt(5, course.getView_count());
			stm.setFloat(6, course.getPrice());
			stm.setInt(7, course.getDiscount());
			stm.setFloat(8, course.getPromotion_price());
			stm.setString(9, course.getDescription());
			stm.setString(10, course.getContent());
			stm.setInt(11, course.getCategory_id());
			stm.setDate(12, course.getLast_update());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
