package com.myclass.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.myclass.connection.JDBCConnection;
import com.myclass.entity.User;
import com.myclass.repository.UserRepository;

@Repository
public class UserRepositoryImpl implements UserRepository {

	public List<User> getListUser() {
		List<User> listUser = new LinkedList<User>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				User user = new User();
				user.setId(res.getInt("id"));
				user.setEmail(res.getString("email"));
				user.setPassword(res.getString("password"));
				user.setRole_id(res.getInt("role_id"));
				listUser.add(user);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listUser;
	}

	public int addNewUser(User user) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO users (fullname,email,password,role_id,avatar,phone,address) VALUES (?,?,?,?,?,?,?);";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, user.getFullname());
			stm.setString(2, user.getEmail());
			stm.setString(3, user.getPassword());
			stm.setInt(4, user.getRole_id());
			stm.setString(5, user.getAvatar());
			stm.setString(6, user.getPhone());
			stm.setString(7, user.getAddress());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteUserById(int id) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM users WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public User findUserByEmail(String email) {
		User user = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users WHERE users.email = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, email);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				user = new User();
				user.setId(res.getInt("id"));
				user.setFullname(res.getString("fullname"));
				user.setEmail(email);
				user.setPassword(res.getString("password"));
				user.setRole_id(res.getInt("role_id"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	public User findUserById(int id) {
		User user = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				user = new User();
				user.setId(id);
				user.setFullname(res.getString("fullname"));
				user.setEmail(res.getString("email"));
				user.setPassword(res.getString("password"));
				user.setRole_id(res.getInt("role_id"));
				user.setAddress(res.getString("address"));
				user.setAvatar(res.getString("avatar"));
				user.setPhone(res.getString("phone"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	public int editUser(User user) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE users SET fullname = ?, password = ?, phone = ?, address = ?, avatar = ?, role_id = ? WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, user.getFullname());
			stm.setString(2, user.getPassword());
			stm.setString(3, user.getPhone());
			stm.setString(4, user.getAddress());
			stm.setString(5, user.getAvatar());
			stm.setInt(6, user.getRole_id());
			stm.setInt(7, user.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
