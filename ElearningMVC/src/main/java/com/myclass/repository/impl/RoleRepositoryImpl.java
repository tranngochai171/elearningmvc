package com.myclass.repository.impl;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.myclass.entity.Role;
import com.myclass.repository.RoleRepository;

@Repository
public class RoleRepositoryImpl implements RoleRepository {
	@Autowired
	private SessionFactory sessionFactory;

	public List<Role> getListRole() {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			String hql = "FROM Role";
			Query<Role> query = session.createQuery(hql, Role.class);
			transaction.commit();
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
		} finally {
			session.close();
		}
		return new LinkedList<Role>();
	}

	public int addNewRole(Role role) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.saveOrUpdate(role);
			transaction.commit();
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			return 0;
		} finally {
			session.close();
		}
	}

	public int deleteRoleById(int id) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Role model = session.find(Role.class, id);
			session.remove(model);
			transaction.commit();
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			return 0;
		} finally {
			session.close();
		}
	}

	public Role findById(int id) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Role model = session.find(Role.class, id);
			transaction.commit();
			return model;
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			return null;
		} finally {
			session.close();
		}
	}

	public int editRole(Role role) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.saveOrUpdate(role);
			transaction.commit();
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			transaction.rollback();
			return 0;
		} finally {
			session.close();
		}
	}

}
