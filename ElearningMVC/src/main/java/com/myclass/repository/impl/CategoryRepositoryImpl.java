package com.myclass.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.myclass.connection.JDBCConnection;
import com.myclass.entity.Category;
import com.myclass.repository.CategoryRepository;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {
	@Autowired
	public List<Category> getListCategory() {
		List<Category> listCategory = new LinkedList<Category>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM categories;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				Category category = new Category();
				category.setId(res.getInt("id"));
				category.setIcon(res.getString("icon"));
				category.setTitle(res.getString("title"));
				listCategory.add(category);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listCategory;
	}

	public int addNewCategory(Category category) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "INSERT INTO categories (title,icon) VALUES (?,?);";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, category.getTitle());
			stm.setString(2, category.getIcon());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteCategoryById(int id) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "DELETE FROM categories WHERE categories.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Category findById(int id) {
		Category category = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM categories WHERE categories.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				category = new Category();
				category.setId(id);
				category.setTitle(res.getString("title"));
				category.setIcon(res.getString("icon"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return category;
	}

	public int editCategory(Category category) {
		int result = 0;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "UPDATE categories SET title = ?, icon = ? WHERE categories.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, category.getTitle());
			stm.setString(2, category.getIcon());
			stm.setInt(3, category.getId());
			result = stm.executeUpdate();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
