package com.myclass.repository;

import java.util.List;

import com.myclass.entity.User;

public interface UserRepository {
	public List<User> getListUser();

	public int addNewUser(User user);

	public int deleteUserById(int id);

	public User findUserByEmail(String email);

	public User findUserById(int id);
	
	public int editUser(User user);
}
