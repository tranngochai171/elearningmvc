package com.myclass.repository;

import java.util.List;

import com.myclass.entity.Course;

public interface CourseRepository {
	public List<Course> getListCourse();

	public int addNewCourse(Course course);
}
