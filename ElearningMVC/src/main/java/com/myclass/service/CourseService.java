package com.myclass.service;

import java.util.List;

import com.myclass.entity.Course;

public interface CourseService {
	public List<Course> getListCourse();

	public int addNewCourse(Course course);
}
