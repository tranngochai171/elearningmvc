package com.myclass.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myclass.connection.JDBCConnection;
import com.myclass.dto.UserDto;
import com.myclass.entity.User;
import com.myclass.repository.UserRepository;
import com.myclass.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;

	public List<UserDto> getListUserDto() {
		List<UserDto> listUserDto = new LinkedList<UserDto>();
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users JOIN roles ON users.role_id = roles.id;";
			PreparedStatement stm = conn.prepareStatement(query);
			ResultSet res = stm.executeQuery();
			while (res.next()) {
				UserDto userDto = new UserDto();
				userDto.setId(res.getInt("id"));
				userDto.setFullname(res.getString("fullname"));
				userDto.setEmail(res.getString("email"));
				userDto.setPassword(res.getString("password"));
				userDto.setRole_id(res.getInt("role_id"));
				userDto.setRole_name(res.getString("name"));
				listUserDto.add(userDto);
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listUserDto;
	}

	public int addNewUser(User user) {
		return this.userRepository.addNewUser(user);
	}

	public int deleteUserById(int id) {
		return this.userRepository.deleteUserById(id);
	}

	public User findUserByEmail(String email) {
		return this.userRepository.findUserByEmail(email);
	}

	public UserDto findUserDtoById(int id) {
		UserDto userDto = null;
		try {
			Connection conn = JDBCConnection.getConnection();
			String query = "SELECT * FROM users WHERE users.id = ?;";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setInt(1, id);
			ResultSet res = stm.executeQuery();
			if (res.next()) {
				userDto = new UserDto();
				userDto.setId(id);
				userDto.setFullname(res.getString("fullname"));
				userDto.setEmail(res.getString("email"));
				userDto.setPassword(res.getString("password"));
				userDto.setRole_id(res.getInt("role_id"));
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDto;
	}

	public User findUserById(int id) {
		return this.userRepository.findUserById(id);
	}

	public int editUser(User user) {
		return this.userRepository.editUser(user);
	}

}
