package com.myclass.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myclass.entity.Course;
import com.myclass.repository.CourseRepository;
import com.myclass.service.CourseService;

@Service
public class CourseServiceImpl implements CourseService {
	@Autowired
	private CourseRepository courseRepository;

	public List<Course> getListCourse() {
		return this.courseRepository.getListCourse();
	}

	public int addNewCourse(Course course) {
		return this.courseRepository.addNewCourse(course);
	}

}
