package com.myclass.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myclass.entity.Category;
import com.myclass.repository.CategoryRepository;
import com.myclass.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private CategoryRepository categoryRepository;

	public List<Category> getListCategory() {
		return this.categoryRepository.getListCategory();
	}

	public int addNewCategory(Category category) {
		return this.categoryRepository.addNewCategory(category);
	}

	public int deleteCategoryById(int id) {
		return this.categoryRepository.deleteCategoryById(id);
	}

	public Category findById(int id) {
		return this.categoryRepository.findById(id);
	}

	public int editCategory(Category category) {
		return this.categoryRepository.editCategory(category);
	}

}
