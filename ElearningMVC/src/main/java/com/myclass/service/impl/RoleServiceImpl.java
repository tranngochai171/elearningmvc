package com.myclass.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myclass.entity.Role;
import com.myclass.repository.RoleRepository;
import com.myclass.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleRepository roleRepository;

	public List<Role> getListRole() {
		return this.roleRepository.getListRole();
	}

	public int addNewRole(Role role) {
		return this.roleRepository.addNewRole(role);
	}

	public int deleteRoleById(int id) {
		return this.roleRepository.deleteRoleById(id);
	}

	public Role findById(int id) {
		return this.roleRepository.findById(id);
	}

	public int editRole(Role role) {
		return this.roleRepository.editRole(role);
	}

}
