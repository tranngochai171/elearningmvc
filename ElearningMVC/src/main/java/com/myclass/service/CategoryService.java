package com.myclass.service;

import java.util.List;

import com.myclass.entity.Category;

public interface CategoryService {
	public List<Category> getListCategory();

	public int addNewCategory(Category category);

	public int deleteCategoryById(int id);

	public Category findById(int id);

	public int editCategory(Category category);
}
