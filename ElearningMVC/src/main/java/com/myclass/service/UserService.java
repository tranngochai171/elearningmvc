package com.myclass.service;

import java.util.List;

import com.myclass.dto.UserDto;
import com.myclass.entity.User;

public interface UserService {
	public List<UserDto> getListUserDto();

	public int addNewUser(User user);

	public int deleteUserById(int id);

	public User findUserByEmail(String email);

	public UserDto findUserDtoById(int id);

	public User findUserById(int id);

	public int editUser(User user);

}
