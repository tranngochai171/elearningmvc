<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<div class="main-body">
	<div class="page-body">
		<div class="row">
			<div class="col-md-12 mb-2">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item"><a href="/admin"> <i
								class="feather icon-home"></i> Trang chủ
						</a></li>
						<li class="breadcrumb-item"><a href="#!">Thêm mới tài
								khoản</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						<h2 class="text-uppercase text-center">Thêm mới tài khoản</h2>
						<p class="text-center text-danger">${message }</p>
					</div>
					<div class="card-block">
						<form:form action="add" method="post" modelAttribute="userDto">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Họ tên</label>
										<form:input path="fullname" cssClass="form-control" />
										<form:errors path="fullname" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Email</label>
										<form:input path="email" cssClass="form-control" />
										<form:errors path="email" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Mật khẩu</label>
										<form:password path="password" cssClass="form-control" />
										<form:errors path="password" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Nhập lại mật khẩu</label>
										<form:password path="password_comfirm" cssClass="form-control" />
										<form:errors path="password_comfirm" cssClass="text-danger" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Điện thoại (Optional)</label>
										<form:input path="phone" cssClass="form-control" />
									</div>
									<div class="form-group">
										<label>Địa chỉ (Optional)</label>
										<form:input path="address" cssClass="form-control" />
									</div>
									<div class="form-group">
										<label>Avatar (Optional)</label>
										<form:input path="avatar" cssClass="form-control" />
									</div>
									<div class="form-group">
										<label>Loại người dùng</label>
										<form:select path="role_id" items="${listRole }"
											itemLabel="name" itemValue="id" cssClass="form-control" />
									</div>
								</div>
								<div class="col-12 mt-3">
									<div class="form-group">
										<button type="submit" class="btn btn-primary m-b-0">Lưu
											lại</button>
										<a href='<c:url value="<%=UrlConstants.URL_USER%>"/>'
											class="btn btn-secondary text-white">Quay lại</a>
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>