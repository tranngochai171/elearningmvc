<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><tiles:insertAttribute name="title" /></title>
<tiles:insertAttribute name="style" />
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="error-template">
					<h1>Oops!</h1>
					<!-- BODY CONTENT -->
					<tiles:insertAttribute name="body" />
					<!-- END BODY CONTENT -->
					<div class="error-actions">
						<a href='<c:url value="<%=UrlConstants.URL_CATEGORY%>"/>'
							class="btn btn-primary btn-lg"><span
							class="glyphicon glyphicon-home"></span> Take Me Home </a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<tiles:insertAttribute name="script" />
</body>
</html>