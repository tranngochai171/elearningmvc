<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="main-body">
	<div class="page-body">
		<div class="row">
			<div class="col-md-12 mb-2">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item"><a href="/admin"> <i
								class="feather icon-home"></i> Trang chủ
						</a></li>
						<li class="breadcrumb-item"><a href="/admin/role">Quyền</a></li>
						<li class="breadcrumb-item"><a href="#!">Thêm mới quyền</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						<h2 class="text-uppercase text-center">Thêm mới quyền</h2>
						<p class="text-center text-danger">${message }</p>
					</div>
					<div class="card-block">
						<form:form action="add" method="post" modelAttribute="role">
							<div class="row">
								<div class="col-md-6 m-auto">
									<div class="form-group">
										<label>Tên quyền</label>
										<form:input path="name" id="name" cssClass="form-control" />
										<form:errors path="name" cssClass="text-danger" />
									</div>
									<div class="form-group">
										<label>Mô tả</label>
										<form:input path="description" id="desc"
											cssClass="form-control" />
										<form:errors path="description" cssClass="text-danger" />
									</div>
									<div class="form-group mt-3">
										<button type="submit" class="btn btn-primary m-b-0">Lưu
											lại</button>
										<a href='<c:url value="<%=UrlConstants.URL_ROLE%>"/>'
											class="btn btn-secondary text-white">Quay lại</a>
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>