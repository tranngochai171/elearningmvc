<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src='<c:url value="/plugins/jquery/js/jquery.min.js"/>'></script>
<script type="text/javascript"
	src='<c:url value="/plugins/jquery-ui/js/jquery-ui.min.js"/>'></script>
<script type="text/javascript"
	src='<c:url value="/plugins/popper.js/js/popper.min.js"/>'></script>
<script type="text/javascript"
	src='<c:url value="/plugins/bootstrap/js/bootstrap.min.js"/>'></script>
<!-- jquery slimscroll js -->
<script type="text/javascript"
	src='<c:url value="/plugins/jquery-slimscroll/js/jquery.slimscroll.js"/>'></script>
<!-- modernizr js -->
<script type="text/javascript"
	src='<c:url value="/plugins/modernizr/js/modernizr.js"/>'></script>
<script type="text/javascript"
	src='<c:url value="/plugins/modernizr/js/css-scrollbars.js"/>'></script>
<script src='<c:url value="/js/pcoded.min.js"/>'></script>
<script src='<c:url value="/js/vartical-layout.min.js"/>'></script>
<script
	src='<c:url value="/js/jquery.mCustomScrollbar.concat.min.js"/>'></script>
<!-- Custom js -->
<script type="text/javascript"
	src='<c:url value="/js/script.js"/>'></script>
<script src="//cdn.ckeditor.com/4.11.4/full/ckeditor.js"></script>
<script
	src="http://cdn.ckeditor.com/4.4.7/standard-all/adapters/jquery.js"></script>