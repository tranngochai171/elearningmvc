<%@page import="com.myclass.util.UrlConstants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<nav class="pcoded-navbar">
	<div class="pcoded-inner-navbar main-menu">
		<ul class="pcoded-item pcoded-left-item">
			<li class=""><a href="javascript:void(0)"> <span
					class="pcoded-micon"><i class="feather icon-home"></i></span> <span
					class="pcoded-mtext">Dashboard</span>
			</a></li>
			<li class=""><a
				href='<c:url value="<%=UrlConstants.URL_CATEGORY%>"/>'> <span
					class="pcoded-micon"><i class="feather icon-layers"></i></span> <span
					class="pcoded-mtext">Danh mục</span>
			</a></li>
			<li class=""><a
				href='<c:url value="<%=UrlConstants.URL_COURSE%>"/>'> <span
					class="pcoded-micon"><i class="feather icon-layers"></i></span> <span
					class="pcoded-mtext">Khóa học</span>
			</a></li>
			<li class=""><a
				href='<c:url value="<%=UrlConstants.URL_VIDEO%>"/>'> <span
					class="pcoded-micon"><i class="feather icon-layers"></i></span> <span
					class="pcoded-mtext">Video</span>
			</a></li>
			<li class=""><a
				href='<c:url value="<%=UrlConstants.URL_TARGET%>"/>'> <span
					class="pcoded-micon"><i class="feather icon-layers"></i></span> <span
					class="pcoded-mtext">Mục tiêu</span>
			</a></li>
			<li class=""><a
				href='<c:url value="<%=UrlConstants.URL_USER%>"/>'> <span
					class="pcoded-micon"><i class="feather icon-user"></i></span> <span
					class="pcoded-mtext">Tài khoản</span>
			</a></li>
			<li class=""><a
				href='<c:url value="<%=UrlConstants.URL_ROLE%>"/>'> <span
					class="pcoded-micon"><i class="feather icon-user"></i></span> <span
					class="pcoded-mtext">Quyền</span>
			</a></li>
		</ul>
	</div>
</nav>